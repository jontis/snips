import numpy as np
import re
import itertools
from collections import Counter
import json
import pandas as pd
from joblib import Parallel, delayed, Memory

data_path_gen = '/home/jonathan/code/snips/dataGenerated/'
data_path_source = '/home/jonathan/code/snips/dataSource/'

cache = Memory(cachedir=data_path_gen, compress=False)

@cache.cache
def ReadSourceData():
    with open(data_path_source + 'subtitles_dl_challenge_data.json') as json_data:
        d = json.load(json_data)
    
    df_source = pd.DataFrame.from_dict(d[0])
    for i in d:
        df_source  = df_source.append(i, ignore_index=True)
        
    df_source.rename(index=str, 
                 columns={"line": "li_now", "next_line": "li_post", "previous_line": "li_pre", 
                 "tagged_places": "place", "user_is_intending_to_go_there": "user_go", 
                 "user_is_there": "user_is"}, inplace=True)    
    df_source.drop('id', axis=1, inplace=True) #drop col id, whe use pd index now

    # fix tagging
    df_source.user_go[[i is None for i in df_source.user_go.values]] = False
    df_source.user_is[[i is None for i in df_source.user_is.values]] = False
    
    # combine lines
    df_source['li_full'] = ''
    def row_combine(row):
        #return row['li_pre'] + ' ' + row['li_now'] + ' ' + row['li_post']
        return row['li_now']
    df_source['li_full'] = df_source.apply(row_combine, axis=1)
    df_source.drop(['li_pre', 'li_now', 'li_post'], axis=1, inplace=True) # drop old lines
    
    df_source['li_clean'] = ''
    # replace tagged places name with 'place' to give our model a chance
    def row_place(row):
        if row['place'] == []:
            return row['li_full']
        else:
            li_out = row['li_full']
            for i in row['place']:
                place = i['word']
                li_out = li_out.replace(place, 'PLACE')             
            return li_out 
    df_source['li_clean'] = df_source.apply(row_place, axis=1)
        
    def row_clean(row):
        # strip unwanted signs
        row_out = row['li_clean'].replace('-', '')
        row_out = row_out.replace('.', '')
        row_out = row_out.replace('?', '')
        row_out = row_out.replace(',', '')
        row_out = row_out.replace('  ', ' ')
        row_out = row_out.replace('  ', ' ')
        row_out = row_out.lower()
        return row_out
    df_source['li_clean'] = df_source.apply(row_clean, axis=1)
    return df_source


def clean_str(string):
    """
    Tokenization/string cleaning for all datasets except for SST.
    Original taken from https://github.com/yoonkim/CNN_sentence/blob/master/process_data.py
    """
    string = re.sub(r"[^A-Za-z0-9(),!?\'\`]", " ", string)
    string = re.sub(r"\'s", " \'s", string)
    string = re.sub(r"\'ve", " \'ve", string)
    string = re.sub(r"n\'t", " n\'t", string)
    string = re.sub(r"\'re", " \'re", string)
    string = re.sub(r"\'d", " \'d", string)
    string = re.sub(r"\'ll", " \'ll", string)
    string = re.sub(r",", " , ", string)
    string = re.sub(r"!", " ! ", string)
    string = re.sub(r"\(", " \( ", string)
    string = re.sub(r"\)", " \) ", string)
    string = re.sub(r"\?", " \? ", string)
    string = re.sub(r"\s{2,}", " ", string)
    return string.strip().lower()


def load_data_and_labels():
    """
    Loads MR polarity data from files, splits the data into words and generates labels.
    Returns split sentences and labels.
    """
    # Load data from files
    df_source = ReadSourceData()
    #positive_examples = list(open("./data/rt-polaritydata/rt-polarity.pos", "r").readlines())
    positive_examples = df_source[df_source.user_go == True].li_clean.values.tolist()
    positive_examples = [s.strip() for s in positive_examples]
    pos_n = len(positive_examples)
    #negative_examples = list(open("./data/rt-polaritydata/rt-polarity.neg", "r").readlines())
    negative_examples = df_source[df_source.user_go == False].li_clean.values.tolist()
    negative_examples = [s.strip() for s in negative_examples]
    
    # balance sets
    negative_examples = negative_examples[:pos_n]
    
    # Split by words
    x_text = positive_examples + negative_examples
    x_text = [clean_str(sent) for sent in x_text]
    # Generate labels
    positive_labels = [[0, 1] for _ in positive_examples]
    negative_labels = [[1, 0] for _ in negative_examples]
    y = np.concatenate([positive_labels, negative_labels], 0)
    return [x_text, y]


def batch_iter(data, batch_size, num_epochs, shuffle=True):
    """
    Generates a batch iterator for a dataset.
    """
    data = np.array(data)
    data_size = len(data)
    num_batches_per_epoch = int(len(data)/batch_size) + 1
    for epoch in range(num_epochs):
        # Shuffle the data at each epoch
        if shuffle:
            shuffle_indices = np.random.permutation(np.arange(data_size))
            shuffled_data = data[shuffle_indices]
        else:
            shuffled_data = data
        for batch_num in range(num_batches_per_epoch):
            start_index = batch_num * batch_size
            end_index = min((batch_num + 1) * batch_size, data_size)
            yield shuffled_data[start_index:end_index]
